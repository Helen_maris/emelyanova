%matplotlib inline
import matplotlib.pyplot as plt
import csv
from textblob import TextBlob
import pandas
import sklearn
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC, LinearSVC
from sklearn.metrics import classification_report, f1_score, accuracy_score, confusion_matrix
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import StratifiedKFold, cross_val_score, train_test_split 
from sklearn.model_selection import StratifiedKFold, cross_val_score, train_test_split
from sklearn.tree import DecisionTreeClassifier 
from sklearn.learning_curve import learning_curve
from nltk import word_tokenize
import nltk
ps = nltk.stem.PorterStemmer()
sno = nltk.stem.SnowballStemmer('english')
lemma = nltk.wordnet.WordNetLemmatizer()
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))


# https://archive.ics.uci.edu/ml/datasets/SMS+Spam+Collection

path = '/Users/elenaemelanova/Downloads/smsspamcollection/SMSSpamCollection'

messages = pandas.read_csv(path, sep='\t', names=["label", "message"])
#print(messages)

#Датасет несбалансирован, наблюдений класса ham значительно больше (4825), чем класса spam (747). Если использовать dummy classifier, который всегда будет предсказывать класс ham, то в #большинстве случаев предсказание будет верным, так как ham встречается в 86% случаев. Но для определения спама это нехорошо, потому что спам никогда не будет определяться


print(messages.groupby('label').describe())
messages['length'] = messages['message'].map(lambda text: len(text))
#print(messages.head())


#new = {'message': pd.Series([]), 'label' : pd.Series([])}
new_messages = pd.get_dummies(messages['label'])
df_new = pd.concat([messages, new_messages], axis=1)
for i in df_new['ham']:
    
    print(df_new['label'])
        


X = np.array([[1., 0.], [2., 1.], [0., 0.]])
y = np.array([0, 1, 2])
​
from scipy.sparse import coo_matrix
X_sparse = coo_matrix(X)
​
print(X_sparse)
​
  (0, 0)	1.0
  (1, 0)	2.0
  (1, 1)	1.0
#Токенизация, со знаками препинания и без


pt_t = {'message': pd.Series([]), 'label' : pd.Series([])}
punct_t = pd.DataFrame(pt_t)
punct = [word_tokenize(msg) for msg in messages["message"]]
punct_t['message'] = punct
punct_t['label'] = messages['label']


punct_t


pt = {'message': pd.Series([]), 'label' : pd.Series([])}
tokens = pd.DataFrame(pt)
sent = []
for i in punct_t['message']:
    sentences = []
    for j in i:
        j = j.lower()
        j = j.strip(',.?!()<>/&*+;:|$%')
        if j != '':
            sentences.append(j)
    sent.append(sentences)
#tokens = messages
tokens['message'] = sent
tokens['label'] = messages['label']


tokens


#Стемминг


st = {'message': pd.Series([]), 'label' : pd.Series([])}
stems = pd.DataFrame(st)
sent_stems = []
for i in punct_t['message']:
    stem = []
    for j in i:
        j = j.lower().strip(',.?!()<>/&*+;:|$%')
        st = ps.stem(j)
        if j != '':
            stem.append(st)
    sent_stems.append(stem)
​
stems['message'] = sent_stems
stems['label'] = messages['label']
In [19]:

stems

#Лемматизация


sl = {'message': pd.Series([]), 'label' : pd.Series([])}
lemmas = pd.DataFrame(sl)
sent_lemmas = []
for sentence in punct_t['message']:
    lem = []
    for word in sentence:
        word = word.lower().strip(',.?!()<>/&*+;:|$%')
        l = lemma.lemmatize(word)
        if l != '':
            lem.append(l)
    sent_lemmas.append(lem)
​
lemmas['message'] = sent_lemmas
lemmas['label'] = messages['label']


stems


#Удаление стоп-слов


stw = {'message': pd.Series([]), 'label' : pd.Series([])}
stopw_lemmas = pd.DataFrame(stw)
stw_lemmas = []
for i in punct_t['message']:
    stw = []
    for j in i:
        if j not in stop:
            stw.append(j)
    stw_lemmas.append(stw)
stopw_lemmas['message'] = stw_lemmas
stopw_lemmas['label'] = messages['label']  


stopw_lemmas


#Пороги минимальной и максимальной документной частоты. Я расскласифицировала леммы по частоте, отсортировала частоты и в остортированных частотах убрала первые и последние 10% 


count_lemmas = {}
for i in lemmas['message']:
    stwords = []
    for j in i:
        if j in count_lemmas.keys():
            t = count_lemmas[j]
            t += 1
            count_lemmas[j] = t
        else:
            count_lemmas[j] = 1
counts = []
deleted_words = []
deleted_numbers = []
for i in sorted(count_lemmas.values()):
    if i not in counts:
        counts.append(i)
c = int(len(counts)/100*10)
p = len(counts) - c
for number in range(c, p):
    deleted_numbers.append(counts[number-1])
​
for i in deleted_numbers:
    for j in count_lemmas:
        if count_lemmas[j] == i:
            deleted_words.append(j)
deleted_words
